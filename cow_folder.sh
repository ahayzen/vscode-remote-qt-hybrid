#!/bin/bash

# SPDX-FileCopyrightText: 2021 Andrew Hayzen <ahayzen@gmail.com>
#
# SPDX-License-Identifier: MPL-2.0

# The script is useful for making folders on the host that you want to mount
# as read-only into docker but you want any changes inside docker to persist
# as a diff.
#
# For example if you are mounting the system's Qt and you want to install
# custom Qt modules inside the docker container without affecting the system.
#
# Note that you need to run this script on the host before opening the container.
#
# Usage: ./cow_folder.sh /readonly/path cow-name
#
# This will create a COW folder at ./.cow/cow-name-merged

set -e

# Extract target folder and folder name
READONLY_PATH="$1"
COW_NAME="$2"

if [[ -z "$READONLY_PATH" || -z "$COW_NAME" ]]; then
  echo "Usage: ./cow_folder.sh /readonly/path cow-name"
  echo ""
  echo "Which results in a COW folder at ./.cow/cow-name-merged"
  exit 1
fi

# Find current path
SCRIPT=$(realpath "$0")
SCRIPTPATH=$(dirname "$SCRIPT")

# Define COW folders
COW_FOLDER="${SCRIPTPATH}/.cow/${COW_NAME}"
DIFF_PATH="${COW_FOLDER}-diff"
TMP_PATH="${COW_FOLDER}-tmp"
MERGED_PATH="${COW_FOLDER}-merged"

# Create COW folders
(
  set -x
  mkdir -p "$DIFF_PATH"
  mkdir -p "$TMP_PATH"
  mkdir -p "$MERGED_PATH"
)

# Mount COW filesystem of READONLY_PATH into MERGED_PATH
(
  set -x
  sudo mount -t overlay overlay -olowerdir="$READONLY_PATH",upperdir="$DIFF_PATH",workdir="$TMP_PATH" "$MERGED_PATH"
)

# Wait for user to finish
echo ""
read -r -p "Mounted readonly path as COW filesystem. Press enter to unmount"

# Unmount COW filesystem
(
  set -x
  sudo umount "$MERGED_PATH"
)
