// SPDX-FileCopyrightText: 2018, 2019, 2020 Andrew Hayzen <ahayzen@gmail.com>
//
// SPDX-License-Identifier: CC0-1.0

#include <QtWidgets>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    QWidget window;
    window.resize(300, 300);
    window.setWindowTitle("Hello World");
    window.show();

    return app.exec();
}
