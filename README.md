<!--
SPDX-FileCopyrightText: 2020 Andrew Hayzen <ahayzen@gmail.com>

SPDX-License-Identifier: CC0-1.0
-->

# Qt Hybrid Setup

This uses Qt installed via the online installer on the host and mounts the Qt libraries into the container.

This has the advantage of not needing to install Qt headless within the container so one can use the normal Qt maintenance tools
and therefore results in a smaller container. But with the disadvantage of a non fully unattended install as the user needs to
install Qt.

If you want Qt embedded into the container see the [non-hybrid remote containers variant](https://gitlab.com/ahayzen/vscode-remote-qt).

## Modifying Qt only in the container

Use the `cow_folder.sh` script to mount the Qt folder from the host but with COW inside the container.

For example, when mounting the system's Qt as read-only and you want to install additional modules
inside the container without affecting the system. Run the following command

```
./cow_folder.sh /host/qt qt-cow
```

This will then create a folder in the directory of the script called `.cow/qt-cow-merged` that you can mount into your docker container.

# Try Out Development Containers: Qt Linux on Linux

This is a sample project that lets you try out the **[VS Code Remote - Containers](https://aka.ms/vscode-remote/containers)** extension in a few easy steps.

> **Note:** This sample project only works with Linux as the host, because it shares the X11 socket with the host.

## Setting up the development container

Follow these steps to open this sample in a container:

1. If this is your first time using a development container, please follow the [getting started steps](https://aka.ms/vscode-remote/containers/getting-started).

2. Open `.devcontainer/devcontainer.json`
    - Ensure the location to the version of Qt you are using is correct in the `runArgs`
    - After changing the location ensure that the `PATH` to Qt is correct in the `remoteEnv`
    - Make any other changes, such as ccache, Nvidia driver permissions, any other permission changes

3. Open `.devcontainer/Dockerfile`
    - Add to the end any extra packages your project needs
    - Change the `debian_chroot` to the name of your project
    - If you are using Nvidia drivers uncomment Nvidia enablement lines at the top and bottom of the file

4. If you're not yet in a development container:
   - Clone this repository.
   - Press <kbd>F1</kbd> and select the **Remote-Containers: Open Folder in Container...** command.
   - Select the cloned copy of this folder, wait for the container to start, and try things out!

## Things to try

Once you have this sample opened in a container, you'll be able to work with it like you would locally.

Some things to try:

1. **Edit:**
   - Open `main.cpp`
   - Try adding some code and check out the language features.
1. **Terminal:** Press <kbd>ctrl</kbd>+<kbd>shift</kbd>+<kbd>\`</kbd> and type `uname` and other Linux commands from the terminal window.
1. **Build, Run, and Debug:**
   - Open `main.cpp`
   - Add a breakpoint (e.g. on line 9).
   - Press <kbd>F5</kbd> to launch the app in the container.
   - Once the breakpoint is hit, try hovering over variables, examining locals, and more.
